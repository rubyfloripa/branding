# Branding

Nosso logotipo é formado pela composição do trabalho de [Jonathan Patterson](https://www.iconfinder.com/icons/294395/diamond_expensive_gem_gemstone_jewel_precious_ruby_stone_value_icon) licenciado como _"Free for comercial use"_, juntamente com elementos que remetem a símbolos característicos de Florianópolis, representados pelas ondas, conforme constam no [Brasão da Cidade](https://commons.wikimedia.org/wiki/File:Bandeira_de_Florian%C3%B3polis.svg), e pela Ponte Hercílio luz ao fundo.

O círculo traz a idéia de unidade (comunidade), e pode ser representado em outras cores além da definida atualmente,
respeitando-se o contraste adequado para manter o destaque do Rubi.
